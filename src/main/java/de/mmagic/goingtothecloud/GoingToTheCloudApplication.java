package de.mmagic.goingtothecloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GoingToTheCloudApplication {

	public static void main(String[] args) {
		SpringApplication.run(GoingToTheCloudApplication.class, args);
	}
}
